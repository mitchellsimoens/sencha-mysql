module.exports = {
    get Batch () {
        return require('./Batch');
    },

    get Connection () {
        return require('./Connection');
    },

    get DBable () {
        return require('./DBable');
    },

    get Manager () {
        return require('./Manager');
    },

    get Query () {
        return require('./Query');
    }
};
